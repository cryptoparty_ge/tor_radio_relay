from debian:stretch-slim
run apt-get update && apt-get install -y icecast2
run sed -i '112s/.*//' /etc/icecast2/icecast.xml
run sed -i '122s/.*//' /etc/icecast2/icecast.xml
run sed -i 's/ENABLE=false/ENABLE=true/' /etc/default/icecast2
copy docker-entrypoint.sh /app/docker-entrypoint.sh
run chmod +x /app/docker-entrypoint.sh
user icecast2
entrypoint ["/app/docker-entrypoint.sh"]
cmd icecast2 -c /etc/icecast2/icecast.xml
