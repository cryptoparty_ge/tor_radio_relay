#!/bin/bash

sed -i "s/<server>.*/<server>$SERVER<\/server>/" /etc/icecast2/icecast.xml
sed -i "115s/<port>.*/<port>$PORT<\/port>/" /etc/icecast2/icecast.xml
sed -i "116s/.*/<mount>\/$MOUNT<\/mount>/" /etc/icecast2/icecast.xml
sed -i "117s/.*/<local-mount>\/$MOUNT<\/local-mount>/" /etc/icecast2/icecast.xml
sed -i "s/hackme/$PASSWORD/" /etc/icecast2/icecast.xml

exec "$@"
