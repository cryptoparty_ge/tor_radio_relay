# tor_radio_relay
Tor radio relay

Tested on Linux, should work on Windows and Mac

- Install [Docker Compose](https://docs.docker.com/compose/install/#install-compose)

- Clone this repository

```
git clone https://framagit.org/cryptoparty_ge/tor_radio_relay
cd tor_radio_relay
```

- Replace the `PASSWORD` variable in the `docker-compose.yml` file, by default set to `hackme`, e.g. with `sed`

```
sed -i 's/hackme/mysuperstrongpassword/' docker-compose.yml
```

- Set the `SERVER`, `PORT`, and `MOUNT` variables, by default set to `streaming.parleur.net`, `8000`, and `RadioKlaxon`

- Start the docker containers

```
docker-compose up -d
```

- Get the generated onion address from the first line of the Tor container logs

```
docker logs torradiorelay_tor_1
```

# Notes

You may have to open and forward the 9050 port from your router.
